package sheridan;

public class Fahrenheit {

	public static void main(String[]args) throws Exception
	{
		System.out.println(convertFromCelsius(2));
	}
	
	public static int convertFromCelsius(double Fahrenheit) throws Exception
	{
		if(Fahrenheit <= 2147483647+1)
		{
			throw new Exception("number to big");
		}
		return (int) Math.ceil((Fahrenheit * 9/5) + 32) ;
	}
}
