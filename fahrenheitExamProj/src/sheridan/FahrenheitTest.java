package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FahrenheitTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void regularTest() throws Exception {
		int check = Fahrenheit.convertFromCelsius(20);
		assertTrue("The value is", check == 68);
	}
	@Test(expected = Exception.class)
	public void exceptionTest() throws Exception {
		int check = Fahrenheit.convertFromCelsius(2147483647+1);
		assertFalse("The value is", check == -2147483648);
	}
	@Test
	public void BoundryInTest() throws Exception {
		int check = Fahrenheit.convertFromCelsius(0);
		assertTrue("The value is", check == 32);
	}
	@Test
	public void BoundryOutTest() throws Exception {
		int check = Fahrenheit.convertFromCelsius(0.9);
		assertTrue("The value is", check == 34);
	}
	

}
